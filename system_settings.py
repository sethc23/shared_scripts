from os                 import path         as  os_path
from uuid               import getnode      as  get_mac
macs = {'ub1'           :   '117637351435',
        'ub2'           :   '105773427819682',
        'mbp2'          :   '220083054034723',
        'ec2'           :   '11103791088178',
        'ub3'           :   '11860373353693',
        'ub4'           :   '110905317783',
        'ub5'           :   '110905301719'}

this_mac                =   str(get_mac())
try:
    THIS_PC             =   macs.keys()[macs.values().index(this_mac)]
except:
    print "MAC ADDRESS NOT KNOWN"
    raise SystemError

if   ['mbp2','ub1','ub4','ub5'].count(THIS_PC):
    DB_HOST             =   '10.0.1.52'
    DB_PORT             =   '8800'
elif THIS_PC=='ub2':
    DB_HOST             =   '0.0.0.0'
    DB_PORT             =   '8800'
elif THIS_PC=='ub3':
    DB_HOST             =   'aporo.gotdns.com'
    DB_PORT             =   '8891'

from subprocess         import Popen            as sub_popen
from subprocess         import PIPE             as sub_PIPE
cmd                     =   "bash -l -c 'source ~/.bashrc; get_my_ip_int;'"
p                       =   sub_popen(cmd,stdout=sub_PIPE,shell=True)
(_out,_err)             =   p.communicate()
assert _err is None
THIS_IP                 =   _out.strip('\n')

DB_NAME                 =   'system'
DB_USER                 =   'postgres'
DB_PW                   =   ''
BASE_DIR                =   os_path.dirname(os_path.dirname(__file__))
PASS                    =   'money'
SYS_HEALTH_TXT_ERRORS   =   True
RABBITMQ_CPU_LIMIT      =   20
ELASTIC_SEARCH_CPU_LIMIT=   20
SYSLOG_CPU_LIMIT        =   20
KIBANA_CPU_LIMIT        =   20

TMP_WEB_PAGE            =   '/home/ub2/SERVER2/aprinto/static/phantom_shot'